#Kloonaa remote repo

git clone <repo>

#Luo uusi repo

git init

#Tarkasta tilanne

git status

#Stagea uudet tiedostot ja muutokset

git add .

#Committaa muutokset

git commit -m "Kuvaava viesti"

#Listaa branchit

git branch

#Luo uusi branchi ja vaihda siihen. Uusi branchi ei ilman erillistä committia siirry ulkoiseen git serveriin

git checkout -b <branch>

#Mergeä tämänhetkiseen branchiin (typistä merge yhteen committiin)

git merge <toinen branch> (--no-ff)

#Listaa commitit

git log